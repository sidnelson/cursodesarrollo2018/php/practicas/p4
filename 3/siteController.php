<?php
        
    function actionFormulario(){
        return[
            "vista"=>"formulario.php"
        ];
    }

    function actionMedia(){
        $datos=$_REQUEST['numeros'];
        /**
         * compruebo si es array o string para saber de donde vengo
         */
        if(gettype($datos)=="string"){
            $datos= explode("-", $_REQUEST['numeros']);    
        }
        
        $resultado=0;
        foreach ($datos as $v){
            $resultado+=$v;
        }
        $resultado/=count($datos);
        $datos=implode("-",$datos);
        /*
         * antes de mandar a la vista los numeros los convierto en
         * un string
         */
        
        return[
            'vista'=>'resultado.php',
            'mensaje'=>'La media calculada es:',
            'resultado'=>$resultado,
            'numeros'=>$datos,
        ];
    }
    
    function actionModa(){
        return[
            
        ];
    }
    
    function actionMediana(){
        $datos=$_REQUEST['numeros'];
        $resultado=[]; 
        sort($datos);  
 
        echo implode(", ",$datos) . "<br>" ;  
         
        $cantidad = count($datos);
        
        if(($cantidad%2)!=0){
        	$resultado=$datos[($cantidad/2)];
        }else{
        	$resultado=($datos[($cantidad/2)]+$datos[($cantidad/2)-1])/2;
        }
        
        //$resultado = (($cantidad )/2);
      
        
        return[
            'vista'=>'resultado.php',
            'mensaje'=>'La mediana calculada es: ',
            'resultado'=>$resultado,
            'numeros'=>$datos, 
        ];
    }
    
    function actionDesviacion(){
        return[
            
        ];
    }

        if (isset($_REQUEST["boton"])) {
            switch ($_REQUEST["boton"]){
                case 'media':
                    $datos= actionMedia();
                    break;
                case 'moda':
                    $datos=actionModa();
                    break;
                case 'mediana':
                    $datos=actionMediana();
                    break;
                case 'desviacion':
                    $datos= actionDesviacion();
                    break;
                default:
                    $datos= actionFormulario();
            }
           
        }else{
                 $datos= actionFormulario();
        }

